# API Documentation

##### JavaScript файлы, которые загружены в webview имеют доступ к DeviceController.

---

### Данные от приложения

```javascript
   window.APP_DATA = {
      version, // версия приложения (1.0.1)
      platform: 'pc' || 'android' || 'ios',
      deviceToken: "string",
      languages, // ["en", "ru", ...]
      resolution, // 1920 || 1440 || 1280 || 1024
      user, // см. бд
      cookies, // { a: { name: "a", value: "..."}, b: { name: "b", value: "..." } }
      domain, // см. бд
      project, // см. бд
      instance, // см. бд
      installer, // см. бд (обычный проект)
      newProjectManifest,
      projectManifest,
      serverUrl,
      openedDate, // timestamp последнего открытия приложения
      settings, // json c настройками приложения
      safeAreaInsets, // { "top": 33, "bottom": 0, "left": 0, "right": 0 }
}
```

### Content

1. [Webview](#webview)
2. [Cache](#cache)
3. [FileSystem](#filesystem)
4. [Camera](#camera) - Only mobile
5. [Modal](#modal) - Only mobile
6. [SQL](#sql)
7. [Notifications](#notifications)
8. [Gyroscope](#gyroscope) - Only mobile
9. [Recording](#recording) - Only mobile
10. [User](#user)
11. [System](#system)
12. [Geolocation](#geolocation)
---

1. #### Webview

   - Open html file
     ```javascript
     const path = "project/index.html"; // string (путь до html файла)
     window.DeviceController.webview.open(path);
     ```
   - Open new Window - Only PC
     ```javascript
     const config = {
        url: "https://google.com",
        width: number, // (Default is 1024)
        height: number, // (Default is 768)
        x: number, // (Default is to center the window)
        y: number, // (Default is to center the window)
        resizable: boolean, // (Default is false)
        movable: boolean, // (Default is false)
     };
     window.DeviceController.webview.openWindow(config);
     ```
   - Close new window - Only PC
     ```javascript
     window.DeviceController.webview.closeWindow();
     ```
   - Open project
     ```javascript
     window.DeviceController.webview.openProject();
     ```
   - Reload
     ```javascript
     window.DeviceController.webview.reload();
     ```
   - Go back
      ```javascript
     window.DeviceController.webview.goBack();
     ```
   - Set size - Only PC
     ```javascript
     const size = { x: 1000, y: 500 };
     window.DeviceController.webview.setSize(size);
     ```

2. #### Cache

   - Set value
     ```javascript
     const pathInJSON = "a.b.c"; // можно и так ["a", "b", "c"], "a[0].c"
     const value = "my value"; // любой js type;
     window.DeviceController.cache.set(pathInJSON, value);
     ```
   - Get value
     ```javascript
     const pathInJSON = "a.b.c"; // можно и так ["a", "b", "c"], "a[0].c"
     window.DeviceController.cache.get(pathInJSON).then((value) => {});

     // Чтобы получить весь cache
     window.DeviceController.cache.get().then((value) => {});
     ```
   - Clear
     ```javascript
     window.DeviceController.cache.clear();
     ```

3. #### FileSystem
   - Write file
     ```javascript
     const data = { filePath: "project/index.html", content: "<div>Content</div>" };
     window.DeviceController.fileSystem
       .writeFile(data)
       .then(() => {})
       .catch((error) => {});
     ```
   - Read file
     ```javascript
     const filePath = "project/index.html";
     window.DeviceController.fileSystem
       .readFile(filePath)
       .then((content) => {})
       .catch((error) => {});
     ```
   - Move file
     ```javascript
     const data = { filePath: "project/index.html", destPath: "test/index.html" };
     window.DeviceController.fileSystem
       .moveFile(data)
       .then(() => {})
       .catch((error) => {});
     ```
   - Remove file
     ```javascript
     const filePath = "project/index.html";
     window.DeviceController.fileSystem
       .removeFile(filePath)
       .then(() => {})
       .catch((error) => {});
     ```
   - Create dir
     ```javascript
     const dirPath = "project/test";
     window.DeviceController.fileSystem
       .createDir(dirPath)
       .then(() => {})
       .catch((error) => {});
     ```
   - Read dir (рекурсивное - файлы и папки)
     ```javascript
     const dirPath = "project";
     window.DeviceController.fileSystem
       .readDir(dirPath)
       .then((files) => {
         // files = ["index.html", "test",]
       })
       .catch((error) => {});
     ```
   - Empty dir
     ```javascript
     const dirPath = "project";
     window.DeviceController.fileSystem
       .emptyDir(dirPath)
       .then(() => {})
       .catch((error) => {});
     ```
   - Stat file
     ```javascript
     const path = "project/index.html";
     window.DeviceController.fileSystem
       .statFile(path)
       .then((stat) => {
         /*
          stat = {
           path:            // The same as filepath argument
           ctime: date;     // The creation date of the file
           mtime: date;     // The last modified date of the file
           size: string;    // Size in bytes
           mode: number;    // UNIX file mode
           isFile: () => boolean;        // Is the file just a file?
           isDirectory: () => boolean;   // Is the file a directory?
         };
       */
       })
       .catch((error) => {});
     ```
   - Update (Download new manifest files - PC only)
     ```javascript
     window.DeviceController.fileSystem.update();
     ```
   - Download files
     ```javascript
     const files = [
       {
         url: "https://google.com/myfile.pdf", // url от куда будет скачиваться файл
         name: "myfile.pdf", // имя файла на диске
         dir: "src/documents", // директория файла
       },
       // ... other files
     ];
     const destination = "project"; // корневая папка для всех файлов
     window.DeviceController.fileSystem.download(files, destination);
     ```
   - Subscribe (чтобы получать информацию про скачивание файлов)

     ```javascript
     window.DeviceController.fileSystem.subscribe((message) => {
       /*       
      Есть 5 событий:
     
      1. Начало загрузки файла: 
        message = { 
            event: "file:begin", 
            data: {      
                id: number; // id задачи
                url: string; // url по которому скачивается файл
                name: string; // имя файла
                total: number; // размер файла в байтах
                bytesWritten: 0; // сколько байтов уже было скачано 
                status: "processing"; // статус задачи
                attempt: 1; // номер попытки, чтобы скачать файл
            } 
        };
     
      2. Прогресс загрузки: 
          message = { 
              event: "file:progress", 
              data: {      
                  id: number;
                  url: string;
                  name: string;
                  total: number;
                  bytesWritten: number;
                  status: "processing";
                  attempt: number;
              } 
          };
     
      3. Файл загружен
         message = { 
            event: "file:downloaded", 
            data: {      
                id: number;
                url: string;
                name: string;
                total: number;
                bytesWritten: number;
                status: "done";
                attempt: number;
            } 
        };
     
      4. Ошибка при зарузке файла
         message = { 
            event: "file:error", 
            data: {      
              id: number;
              url: string;
              name: string;
              total: number;
              bytesWritten: number;
              status: "error";
              attempt: number;
              errorMessage: string; // Текст ошибки
            } 
          };
     
      5. Все файлы былы успешно загружаны
         message = { event: "files:downloaded" };      
      */
     });
     ```
4. #### Camera
   - Open
     ```javascript
     const barcodeTypes = ["qr", "code-128", "code-39", "code-93", "codabar", "ean-13", "ean-8", "itf", "upc-e", "pdf-417", "aztec", "data-matrix"];
     const settings = {
       barcode: {
         isActive: boolean,
         types: barcodeTypes, // string array
       },
       position: {
         top: number,
         left: number,
         width: number,
         height: number,
       },
     };
     window.DeviceController.camera.open(settings);
     ```
   - Close
     ```javascript
     window.DeviceController.camera.close();
     ```
   - Take photo
     ```javascript
     // isMakeBase64 - нужно ли сделать из фото base64 и вернуть его
     // isMakeOCR - нужно ли пропустить фото через ocr алгоритм
     // isAutoClose - нужно ли закрыть камеру после того, как было сделано фото
     window.DeviceController.camera
       .takePhoto({ isMakeBase64: true, isMakeOCR: true, isAutoClose: true })
       .then((data) => {
         // data = { photo: { base64: string, name: string, size: number }, ocr: string }
       })
       .catch((error) => {});
     ```
   - Subscribe (чтобы получать информацию про сканирование barcodes)
     ```javascript
     window.DeviceController.fileSystem.subscribe((message) => {
       // message = { event: "camera:barcodes", data: { barcodes: {...} } }
     });
     ```
5. #### Modal

   - Open (PDF файлы)

     ```javascript
     // 1. Для файла на устройстве
     const type = "pdf";
     const url = "project/my.pdf";
     const style = { top: 16, left: 20, right: 20, bottom: 20 };
     window.DeviceController.modal.open({ type: type, url: url, style: style });

     // 2. Для URL
     const type = "url";
     const url = "http://www.google.com/sample.pdf";
     const style = { top: 16, left: 20, right: 20, bottom: 20 };
     window.DeviceController.modal.open({ type: type, url: url, style: style });
     ```

   - Close
     ```javascript
     window.DeviceController.modal.close();
     ```

6. #### SQL

   - Execute
     ```javascript
     const sql = "SELECT * FROM Employees a, Departments b WHERE a.department = b.department_id";
     window.DeviceController.sql
       .execute(sql)
       .then((data) => {
         // data = { rows: [...] }
       })
       .catch((error) => {});
     ```

7. #### Notifications
   - Show
     ```javascript
     const notification = { title: "Title", body: "Body" };
     window.DeviceController.notifications.show(notification);
     ```
   - Set badge count
     ```javascript
     window.DeviceController.notifications.setBadgeCount(10);
     // clear count
     window.DeviceController.notifications.setBadgeCount(0);
     ```
8. #### Gyroscope

   - Start
     ```javascript
     window.DeviceController.gyroscope.start();
     ```
   - Stop
     ```javascript
     window.DeviceController.gyroscope.stop();
     ```
   - Subscribe
     ```javascript
     window.DeviceController.gyroscope.subscribe((data) => {
       // data = { x, y, z, timestamp }
     });
     ```

9. #### Recording
   - Start
     ```javascript
     window.DeviceController.recording.start();
     ```
   - Stop
     ```javascript
     window.DeviceController.recording.stop();
     ```
   - Subscribe
     ```javascript
     window.DeviceController.recording.subscribe((data) => {
       // data = Buffer
     });
     ```
10. #### User
    - Logout
      ```javascript
      window.DeviceController.user.logout();
      ```
    - Clear all (удаляет кэш, все файлы и очищает бд)
      ```javascript
      window.DeviceController.user.clearAll();
      ```
11. #### System - Only PC
    - Open url
    ```javascript
    window.DeviceController.system.openUrl("https://www.google.com");
    ```

12. #### Geolocation
    - Get position
    ```javascript
    window.DeviceController.geolocation.getCurrentPosition().then((data) => {
        // data = { latitude: number, longitude: number }
    }).catch(error => {
      
    });
    ```
